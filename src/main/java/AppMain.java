import org.joda.time.DateTime;
import org.joda.time.Interval;

/**
 * Created by ymanabe on 2015/06/12.
 */
public class AppMain {

    public static void main(String[] args){

        String format = "yyyy/MM/dd";

        DateTime today = DateTime.now();
        System.out.println("今日の日付:" + today);

        DateTime.Property dayOfMonth = today.dayOfMonth();
        DateTime firstDayOfMonth = dayOfMonth.withMinimumValue();
        System.out.println("月初の日付:" + firstDayOfMonth);

        DateTime lastDayOfMonth = dayOfMonth.withMaximumValue();
        System.out.println("月末の日付:" + lastDayOfMonth);

        DateTime.Property dayOfWeek = today.dayOfWeek();
        DateTime firstDayOfWeek = dayOfWeek.withMinimumValue();
        System.out.println("週初めの日付:" + firstDayOfWeek);

        DateTime lastDayOfWeek = dayOfWeek.withMaximumValue();
        System.out.println("週終わりの日付:" + lastDayOfWeek);

        DateTime dateTime1 = new DateTime(2014, 1, 1, 0, 0);
        DateTime dateTime2 = new DateTime(2014, 2, 1, 0, 0);

        Interval interval1 = new Interval(dateTime1, dateTime2);

        DateTime dateTime3 = new DateTime(2014, 1, 31, 0, 0);
        DateTime dateTime4 = new DateTime(2014, 2, 10, 0, 0);
        Interval interval2 = new Interval(dateTime3, dateTime4);

        DateTime dateTime5 = new DateTime(2014, 2, 2, 0, 0);
        DateTime dateTime6 = new DateTime(2014, 2, 10, 0, 0);
        Interval interval3 = new Interval(dateTime5, dateTime6);


        // 期間に日付が含まれるかは contains で取得できる。
        boolean interval1ContainsToday = interval1.contains(today);
        System.out.printf("interval1[%s - %s]には %s は含まれない。[interval1ContainsToday:%s]\n"
                , interval1.getStart().toString(format)
                , interval1.getEnd().toString(format)
                , today.toString(format)
                , interval1ContainsToday);

        // 2つの期間が連続するかは abuts で取得できる。
        boolean interval1AndInterval2IsAbuts = interval1.abuts(interval2);
        System.out.printf("interval1[%s - %s]とinterval2[%s - %s]は連続しない。[interval1AndInterval2IsAbuts:%s]\n"
                , interval1.getStart().toString(format)
                , interval1.getEnd().toString(format)
                , interval2.getStart().toString(format)
                , interval2.getEnd().toString(format)
                , interval1AndInterval2IsAbuts);

        // 2つの期間に差が存在するかは gap で取得できる。
        Interval gapInterval = interval1.gap(interval3);
        System.out.printf("interval1[%s - %s]とinterval3[%s - %s]は差をもつ。[gapInterval:%s]\n"
                , interval1.getStart().toString(format)
                , interval1.getEnd().toString(format)
                , interval3.getStart().toString(format)
                , interval3.getEnd().toString(format)
                , gapInterval);

        // 2つの期間が連続するかは overlap で取得できる。
        Interval overlapInterval = interval1.overlap(interval2);
        System.out.printf("interval1[%s - %s]とinterval2[%s - %s]は重複期間をもつ。[overlapInterval:%s]"
                , interval1.getStart().toString(format)
                , interval1.getEnd().toString(format)
                , interval2.getStart().toString(format)
                , interval2.getEnd().toString(format)
                , overlapInterval);
    }
}
